// @ts-ignore
import * as AWS from 'aws-sdk-mock';
import { assert } from 'chai';

import { GetItemInput } from 'aws-sdk/clients/dynamodb';
import { DynamodbHelper as DB } from '../';

describe('aws/dynamodb', () => {

  AWS.mock('DynamoDB.DocumentClient', 'batchWrite', (params: any, callback: any) => {
    callback(null, params);
  });

  it('should be extensible', () => {
    const db = new DB('my-table');
    assert.isExtensible(db, 'Not Extensible');
  });

  it('should have correct table name', () => {
    let tableName = 'my-table';
    const db = new DB(tableName);
    assert.equal(db.tableName, tableName, 'The table name is not correct!');
  });

  it('should be able to generate correct batchWrite items', async () => {
    let tableName = 'my-table';
    const items = [];
    for (let i = 0; i < 26; i++) {
      items.push({ blomId: i.toString(), typeId: '222' });
    }
    const db = new DB(tableName);
    const batchWriteItems = db.createChunkedBatchWritePutRequests(items);
    assert.equal(batchWriteItems.length, 2, 'Should create exactly 2 chunked batch writes because original items are 26');
    assert.equal(batchWriteItems[0].RequestItems[tableName].length, 25, 'First chunk should have 25 items!');
    assert.equal(batchWriteItems[1].RequestItems[tableName].length, 1, 'First chunk should have 1 items!');
    let promises = [];
    for (let batchWrite of batchWriteItems) {
      promises.push(db.batchWriteDeluxe(batchWrite));
    }
    const result = await Promise.all(promises);
    assert.property(result[0], 'RequestItems', 'RequestItems is missing!');
    // @ts-ignore
    let batchWriteItem = result[0].RequestItems[tableName][0].PutRequest.Item;
    assert.equal(batchWriteItem, items[0], 'Failed to create correct batchWrite structure!!');
  });

  it('should be able to get an item', async () => {
    let mockedResponse: any = { Item: {} };
    AWS.mock('DynamoDB.DocumentClient', 'get', (params: any, callback: any) => {
      callback(null, mockedResponse);
    });
    let params: GetItemInput = {
      TableName: '',
      Key: {
        blomId: {
          S: '1000765',
        },
        typeId: {
          S: '24'
        }
      }
    };
    const db = new DB('my-table');
    let result = await db.getItem(params);
    assert.exists(result.Item, 'Failed to fetch item from table');
  });
});
