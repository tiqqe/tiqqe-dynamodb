import * as AWS from 'aws-sdk';
import { BatchWriteRequest } from 'aws-sdk/clients/clouddirectory';
import { BatchWriteItemInput, PutRequest, WriteRequest } from 'aws-sdk/clients/dynamodb';
import * as log from 'econ-logger';

AWS.config.update({ region: 'eu-west-1' });
// const AWS = AwsXRay.captureAWS(AwsSdk);

export class DynamodbHelper {
  public tableName: string;
  private dynamoDB: AWS.DynamoDB.DocumentClient;

  /**
   * constructor
   * @param tableName string, an existing DynamoDB table to select with
   * @param dynamoDB AWS.DynamoDB.DocumentClient, instance of DynamoDB Client
   */

  constructor(tableName: string, dynamoDB = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true })) {
    this.tableName = tableName;
    this.dynamoDB = dynamoDB;
  }

  /**
   * Will create an array of chunked batchWrite put request items, each of which are ready to be used in a dynamodb batchWrite operation.
   * @param items an array of any items, must match table schema of course.
   */
  public createChunkedBatchWritePutRequests(items: any[]): BatchWriteItemInput[] {
    log.info({ message: `batchWriteDeluxe starting with ${items.length} items` });

    const batchWritePutRequests = this.createBatchWritePutRequests(items);
    log.info({ message: `Created ${batchWritePutRequests.length} batch write PutRequests` });

    const chunkedRequests: WriteRequest[][] = this.chunk(batchWritePutRequests, 25);
    log.info({ message: `Created ${chunkedRequests.length} chunked requests` });

    let chunkedBatchWriteRequests: BatchWriteItemInput[] = [];
    for (let chunk of chunkedRequests) {
      log.info({ message: `Pushing chunks to dynamodb, chunk size: ${chunk.length}` });
      const batchWriteRequest: BatchWriteItemInput = { RequestItems: {} };
      batchWriteRequest.RequestItems[this.tableName] = chunk;
      chunkedBatchWriteRequests.push(batchWriteRequest);
    }
    return chunkedBatchWriteRequests;
  }

  /**
   * Will recursively and with exponential backoff write items to dynamodb. Retries are handled automatically.
   * @param batchWriteRequestItem
   * @param attempt
   */
  public async batchWriteDeluxe(batchWriteRequestItem: AWS.DynamoDB.DocumentClient.BatchWriteItemInput, attempt: number = 0): Promise<any> {
    let delay = Math.floor(Math.random() * Math.pow(2, attempt) * 50); // simple exponential backoff (50 is default base for dynamodb)

    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        try {
          log.info({ message: `will batch write to dynamodb table ${this.tableName}` });
          let result = await this.dynamoDB.batchWrite(batchWriteRequestItem).promise();
          if (result && result.UnprocessedItems && result.UnprocessedItems[this.tableName] && result.UnprocessedItems[this.tableName].length) {
            log.warn({ message: `${result.UnprocessedItems[this.tableName].length} unprocessed items. Running batchWrite again with exponential backoff. Attempt: ${attempt + 2}` });
            result = await this.batchWriteDeluxe({ RequestItems: result.UnprocessedItems }, attempt++);
          }
          resolve(result);
        } catch (ex) {
          reject({ message: `dynamodb.ts: batchWriteDeluxe Could not write items to database:`, ...ex });
        }
      }, delay);
    });
  }

  /**
   * getItem
   * @param query Key object only
   * @returns Promise, a thenable Promise object
   */
  public getItem(query: AWS.DynamoDB.DocumentClient.GetItemInput) {
    let params: AWS.DynamoDB.DocumentClient.GetItemInput = {
      ...query,
      TableName: this.tableName
    };
    return this.dynamoDB.get(params).promise();
  }

  /**
   * batchGetItem
   * @params keys - array of object: {attribute_name: attribute_value} pair
   * @params attributes (optional) - array of fields to return
   * @returns Promise, a thenable Promise object
   */
  public batchGetItem(keys: any[], attributes: any = []) {
    let request: any = {};
    request[`${this.tableName}`] = {
      Keys: [
        ...keys
      ]
    };

    if (attributes.length) {
      request[`${this.tableName}`].AttributesToGet = [...attributes];
    }

    let batchRequest: AWS.DynamoDB.DocumentClient.BatchGetItemInput = {
      RequestItems: {
        ...request
      }
    };
    return this.dynamoDB.batchGet(batchRequest).promise();
  }

  /**
   * scanItems
   * @param query Key Object, and other expressions for filter are accepted
   * @returns Promise, a thenable Promise object
   */
  public scanItems(query: AWS.DynamoDB.DocumentClient.ScanInput) {
    let params: AWS.DynamoDB.DocumentClient.ScanInput = {
      ...query,
      TableName: this.tableName
    };
    return this.dynamoDB.scan(params).promise();
  }

  /**
   * putItem
   * @params query
   * @returns Promise, a thenable Promise object
   */
  public putItem(query: AWS.DynamoDB.DocumentClient.PutItemInput): Promise<AWS.DynamoDB.DocumentClient.PutItemOutput> {
    let params: AWS.DynamoDB.DocumentClient.PutItemInput = {
      TableName: this.tableName,
      ReturnItemCollectionMetrics: 'SIZE',
      Item: {
        ...query
      }
    };

    return this.dynamoDB.put(params).promise();
  }

  /**
   * Creates BatchWRite
   * @param items
   * @param tableName
   */
  private createBatchWritePutRequests(items: PutRequest[]) {
    return items.map(item => {
      return {
        PutRequest: {
          Item: item
        }
      };
    });
  }

  /**
   * Will split the incoming array into a new array chunked up into whatever size you specify.
   * @param items the original array
   * @param chunkSize number of items in each chunk
   */
  private chunk(items: any[], chunkSize: number): any[] {
    return items.reduce((resultArray, item, index) => {
      const chunkIndex = Math.floor(index / chunkSize);

      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = []; // start a new chunk
      }
      resultArray[chunkIndex].push(item);

      return resultArray;
    }, []);
  }
}
